from io import StringIO
from Diplomacy import diplomacy_solve, diplomacy_read, diplomacy_add_hold_input, diplomacy_add_move_input, \
    diplomacy_add_support_input, diplomacy_eval, remove_invalid_support, diplomacy_write
from unittest import main, TestCase


class TestDiplomacy(TestCase):

    # ----
    # read
    # ----
    def test_reader1(self):
        ls = diplomacy_read("A Madrid Hold")
        self.assertEqual(ls, ["A", "Madrid", "Hold"])

    def test_reader2(self):
        ls = diplomacy_read("B Madrid Move London\n")
        self.assertEqual(ls, ["B", "Madrid", "Move", "London"])

    def test_reader3(self):
        ls = diplomacy_read("B London Support A\n")
        self.assertEqual(ls, ["B", "London", "Support", "A"])

    # -----
    # solve
    # ----- 
    def test_solve1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]")

    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC London\nD Austin")

    def test_solve3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London")

    def test_solve4(self):
        r = StringIO("")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "")

    def test_solve5(self):
        r = StringIO("\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "")

    def test_solve6(self):
        r = StringIO("A Madrid Hold\nB Barcelona Support C\nC London Move Madrid\nD Paris Support C")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Barcelona\nC Madrid\nD Paris")

    def test_solve7(self):
        r = StringIO("A Madrid Support B\nB Barcelona Support A\nC London Move Madrid\nD Paris Support C")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Barcelona\nC [dead]\nD Paris")

    # -----
    # hold
    # -----

    def test_hold1(self):
        scenario = diplomacy_add_hold_input(["A", "Madrid", "Hold"], [{}, {}, {}, {}])
        self.assertEqual(scenario, [{"A": "Madrid"}, {"Madrid": ["A"]}, {"A": 0}, {}])

    def test_hold2(self):
        scenario = diplomacy_add_hold_input(["B", "London", "Hold"],
                                            [{"A": "Madrid", "C": "London"}, {"Madrid": ["A"], "London": ["C"]},
                                             {"A": 0, "C": 0}, {}])
        self.assertEqual(scenario,
                         [{"A": "Madrid", "C": "London", "B": "London"}, {"Madrid": ["A"], "London": ["C", "B"]},
                          {"A": 0, "C": 0, "B": 0}, {}])

    def test_hold3(self):
        scenario = diplomacy_add_hold_input(["B", "London", "Hold"],
                                            [{"A": "Madrid", "B": "London"}, {"Madrid": ["A"], "London": ["B"]},
                                             {"A": 0, "B": 0}, {}])
        self.assertEqual(scenario, [{"A": "Madrid", "B": "London"}, {"Madrid": ["A"], "London": ["B"]},
                                    {"A": 0, "B": 0}, {}])

    # -----
    # move
    # -----
    def test_move1(self):
        scenario = diplomacy_add_move_input(["A", "Madrid", "Move", "London"], [{}, {}, {}, {}])
        self.assertEqual(scenario, [{"A": "London"}, {"London": ["A"]}, {"A": 0}, {}])

    def test_move2(self):
        scenario = diplomacy_add_move_input(["A", "Madrid", "Move", "London"],
                                            [{"A": "London"}, {"London": ["A"]}, {"A": 0}, {}])
        self.assertEqual(scenario, [{"A": "London"}, {"London": ["A"]}, {"A": 0}, {}])

    def test_move3(self):
        scenario = diplomacy_add_move_input(["B", "Madrid", "Move", "London"],
                                            [{"A": "London"}, {"London": ["A"]}, {"A": 0}, {}])
        self.assertEqual(scenario, [{"A": "London", "B": "London"}, {"London": ["A", "B"]}, {"A": 0, "B": 0}, {}])

    # -----
    # support
    # -----
    def test_support1(self):
        scenario = diplomacy_add_support_input(["B", "Madrid", "Support", "A"],
                                               [{"A": "London"}, {"London": ["A"]}, {"A": 0}, {}])
        self.assertEqual(scenario,
                         [{"A": "London", "B": "Madrid"}, {"London": ["A"], "Madrid": ["B"]}, {"A": 1, "B": 0},
                          {"B": "A"}])

    def test_support2(self):
        scenario = diplomacy_add_support_input(["B", "Madrid", "Support", "A"],
                                               [{"A": "London", "B": "Madrid"}, {"London": ["A"], "Madrid": ["B"]},
                                                {"A": 0}, {}])
        self.assertEqual(scenario,
                         [{"A": "London", "B": "Madrid"}, {"London": ["A"], "Madrid": ["B"]}, {"A": 1, "B": 0},
                          {"B": "A"}])

    def test_support3(self):
        scenario = diplomacy_add_support_input(["B", "London", "Support", "A"],
                                               [{"A": "London"}, {"London": ["A"]}, {"A": 0}, {}])
        self.assertEqual(scenario, [{"A": "London", "B": "London"}, {"London": ["A", "B"]}, {"A": 1, "B": 0},
                                    {"B": "A"}])

    def test_support4(self):
        scenario = diplomacy_add_support_input(["B", "London", "Support", "A"],
                                               [{"A": "London", "C": "Austin"}, {"London": ["A"], "Austin": ["C"]},
                                                {"A": 1, "C": 0}, {"C": "A"}])
        self.assertEqual(scenario,
                         [{"A": "London", "C": "Austin", "B": "London"}, {"London": ["A", "B"], "Austin": ["C"]},
                          {"A": 2, "C": 0, "B": 0},
                          {"C": "A", "B": "A"}])

    # -----
    # remove
    # -----
    def test_remove1(self):
        scenario = [{"A": "London", "C": "Austin", "B": "London"}, {"London": ["A", "B"], "Austin": ["C"]},
                    {"A": 2, "C": 0, "B": 0},
                    {"C": "A", "B": "A"}]
        remove_invalid_support(scenario)
        self.assertEqual(scenario,
                         [{"A": "London", "C": "Austin", "B": "London"}, {"London": ["A", "B"], "Austin": ["C"]},
                          {"A": 1, "C": 0, "B": 0},
                          {"C": "A", "B": "A"}])

    def test_remove2(self):
        scenario = [{"A": "London", "B": "London"}, {"London": ["A", "B"]}, {"A": 1, "B": 0}, {"B": "A"}]
        remove_invalid_support(scenario)
        self.assertEqual(scenario, [{"A": "London", "B": "London"}, {"London": ["A", "B"]}, {"A": 0, "B": 0},
                                    {"B": "A"}])

    def test_remove3(self):
        scenario = [{"A": "London", "B": "London"}, {"London": ["A", "B"]}, {"A": 0, "B": 0}, {}]
        remove_invalid_support(scenario)
        self.assertEqual(scenario, [{"A": "London", "B": "London"}, {"London": ["A", "B"]}, {"A": 0, "B": 0}, {}])

    # -----
    # writer
    # -----
    def test_writer1(self):
        w = StringIO()
        res = {"A": "[dead]", "B": "Madrid"}
        diplomacy_write(w, res)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid")

    def test_writer2(self):
        w = StringIO()
        res = {"A": 'London', "B": "Madrid"}
        diplomacy_write(w, res)
        self.assertEqual(w.getvalue(), "A London\nB Madrid")

    def test_writer3(self):
        w = StringIO()
        res = {}
        diplomacy_write(w, res)
        self.assertEqual(w.getvalue(), "")

    # -----
    # eval
    # -----
    def test_eval1(self):
        res = diplomacy_eval([{"A": "London", "B": "London"}, {"London": ["A", "B"]}, {"A": 1, "B": 0},
                              {"B": "A"}])
        self.assertEqual(res, {"A": "[dead]", "B": "[dead]"})

    def test_eval2(self):
        res = diplomacy_eval([{"A": "London", "C": "Austin", "B": "London"}, {"London": ["A", "B"], "Austin": ["C"]},
                              {"A": 2, "C": 0, "B": 0},
                              {"C": "A", "B": "A"}])
        self.assertEqual(res, {"A": "London", "B": "[dead]", "C": "Austin"})

    def test_eval3(self):
        res = diplomacy_eval([{"A": "Madrid", "C": "London", "B": "London"}, {"Madrid": ["A"], "London": ["C", "B"]},
                              {"A": 0, "C": 0, "B": 0}, {}])
        self.assertEqual(res, {"A": "Madrid", "B": "[dead]", "C": "[dead]"})

    def test_eval4(self):
        res = diplomacy_eval([{}, {}, {}, {}])
        self.assertEqual(res, {})

    def test_eval5(self):
        res = diplomacy_eval([{"A": "Madrid", "B": "London"}, {"Madrid": ["A"], "London": ["B"]},
                              {"A": 0, "B": 0}, {}])
        self.assertEqual(res, {"A": "Madrid", "B": "London"})

    def test_eval6(self):
        res = diplomacy_eval([{"A": "London", "B": "London"}, {"London": ["A", "B"]}, {"A": 1, "B": 1},
                              {}])
        self.assertEqual(res, {"A": "[dead]", "B": "[dead]"})


if __name__ == "__main__":
    main()

""" #pragma: no cover
"""